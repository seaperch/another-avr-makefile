#ifndef _MAIN_H_
#define _MAIN_H_

#define __AVR_ATmega8A__
#define F_CPU 1000000UL

#include <avr/io.h>
#include <avr/interrupt.h>

#endif /*_MAIN_H_*/