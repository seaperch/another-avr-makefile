#Makefile for AVR C projects

#Applicaton/project name
TARGET:= AVR_xmpl

#Target MCU
MCU:= atmega8

#Lists of C source files and directories with headers (include directories)
C_SOURCES:= \
Src/main.c

C_INCLUDE_DIRS:= \
Inc

#Optimization flags
CC_OPT:= -Og
LD_OPT:= -Os

#Debug? 1/0
DBG:= 1

#The directory with object and output files
BUILD_DIR:= build

#Compiler and linker flags
CC_FLAGS:= $(CC_OPT) -std=c11 -Wall -Wno-main
LD_FLAGS:= $(LD_OPT) -mmcu=$(MCU) -o

ifeq ($(DBG), 1)
CC_FLAGS+= -g
endif

#Binaries
CC:= avr-gcc
OBJCOPY:= avr-objcopy
SZ:= avr-size

#Objcopy flags
HEX_FLAGS:= -j .text -j .data -O ihex
BIN_FLAGS:= -j .text -j .data -O binary

#List of objects
OBJECTS:= $(addprefix $(BUILD_DIR)/, $(notdir $(C_SOURCES:.c=.o)))

vpath %.c $(sort $(dir $(C_SOURCES)))

all: $(BUILD_DIR)/$(TARGET).elf $(BUILD_DIR)/$(TARGET).hex $(BUILD_DIR)/$(TARGET).bin

$(BUILD_DIR)/%.o: %.c Makefile | $(BUILD_DIR)
	$(CC) -c $(addprefix -I, $(C_INCLUDE_DIRS)) $(CC_FLAGS) $< -o $@

$(BUILD_DIR)/$(TARGET).elf: $(OBJECTS) Makefile | $(BUILD_DIR)
	$(CC) $(OBJECTS) $(INCLUDE) $(LD_FLAGS) $@
	@echo "--- .elf was generated ---"
	@echo ""
	@$(SZ) --mcu=$(MCU) --format=avr $@

$(BUILD_DIR)/$(TARGET).hex: $(BUILD_DIR)/$(TARGET).elf | $(BUILD_DIR)
	$(OBJCOPY) $(HEX_FLAGS) $< $@
	@echo "--- .hex was generated ---"
	@echo ""

$(BUILD_DIR)/$(TARGET).bin: $(BUILD_DIR)/$(TARGET).elf | $(BUILD_DIR)
	$(OBJCOPY) $(BIN_FLAGS) $< $@
	@echo "--- .bin was generated ---"
	@echo ""

$(BUILD_DIR):
	mkdir $@

.PHONY: clean
clean:
	rm -r $(BUILD_DIR)