#include "main.h"       /*Just a "blink" example*/

ISR(TIMER0_OVF_vect)
{
    PORTB ^= 0x01;
}

void main(void)
{
    DDRB |= (1 << PIN0);

    sei();

    TIMSK |= (1 << TOIE0);
    TCNT0 = 0;
    TCCR0 &=~ (0xFF);
    TCCR0 |= (1 << CS02) | (1 << CS00);

    while(1)
    {
        /*NOP*/
    }
}